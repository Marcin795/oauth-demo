package com.example.oauthdemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters

@SpringBootApplication
@EntityScan(basePackageClasses = [OauthDemoApplication::class, Jsr310JpaConverters::class])
class OauthDemoApplication

fun main(args: Array<String>) {
    runApplication<OauthDemoApplication>(*args)
}
