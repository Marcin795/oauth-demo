package com.example.oauthdemo.user.payload

import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank

data class CreateUserRequest(
        @NotBlank val name: String,
        @Min(8) val password: String
)