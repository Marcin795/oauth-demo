package com.example.oauthdemo.user

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.NotBlank

@Entity
data class User(@Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long, @NotBlank var name: String, @NotBlank @JsonIgnore var password: String) {

    constructor(name: String, password: String) : this(0, name, password)

}