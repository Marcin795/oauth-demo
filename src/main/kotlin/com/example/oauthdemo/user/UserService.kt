package com.example.oauthdemo.user

import com.example.oauthdemo.user.exception.UserAlreadyExists
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Service

@Service
class UserService(val userRepository: UserRepository, val passwordEncoder: PasswordEncoder) {

    fun addNewUser(name: String, password: String): User {
        if (userRepository.existsByName(name))
            throw UserAlreadyExists()

        val user = User(name, password)
        user.password = passwordEncoder.encode(password)

        return user
    }

}