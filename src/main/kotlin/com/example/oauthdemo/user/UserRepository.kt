package com.example.oauthdemo.user

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserRepository : JpaRepository<User, Long> {

    fun existsByName(name: String): Boolean

    fun findByName(name: String): Optional<User>

}