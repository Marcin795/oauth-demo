package com.example.oauthdemo.user

import com.example.oauthdemo.user.payload.CreateUserRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/user")
class UserController(val userService: UserService) {

    @GetMapping
    fun getUser(authentication: Authentication): Authentication {
        return authentication
    }

    @PostMapping
    fun createUser(@Valid @RequestBody createUserRequest: CreateUserRequest): ResponseEntity<Any> {
        val user = userService.addNewUser(createUserRequest.name, createUserRequest.password)
        return ResponseEntity.ok(user)
    }

}