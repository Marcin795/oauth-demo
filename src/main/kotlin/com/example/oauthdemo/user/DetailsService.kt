package com.example.oauthdemo.user

import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component

@Component
class DetailsService(val userRepository: UserRepository) : UserDetailsService {

    override fun loadUserByUsername(name: String): UserDetails {
        return userRepository.findByName(name)
                .map { User(it.name, it.password, listOf(SimpleGrantedAuthority("USER"))) }
                .orElseThrow { UsernameNotFoundException(name) }
    }

}