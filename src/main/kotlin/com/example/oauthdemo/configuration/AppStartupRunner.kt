package com.example.oauthdemo.configuration

import com.example.oauthdemo.user.User
import com.example.oauthdemo.user.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class AppStartupRunner(val userRepository: UserRepository, val passwordEncoder: PasswordEncoder) : ApplicationRunner {

    val logger: Logger = LoggerFactory.getLogger(AppStartupRunner::class.java)

    override fun run(args: ApplicationArguments?) {
        logger.info("Dupa dupa dupa")
        if (userRepository.findByName("admin").isEmpty) {
            val admin = User("admin", "dupsko123")
            admin.password = passwordEncoder.encode(admin.password)
            userRepository.save(admin)
        }
    }

}
