package com.example.oauthdemo

import com.example.oauthdemo.user.User
import org.hibernate.Session
import org.hibernate.testing.junit4.BaseCoreFunctionalTestCase
import org.hibernate.testing.transaction.TransactionUtil.doInHibernate
import org.junit.Assert.assertTrue
import org.junit.Test

class HibernateKotlinIntegrationTest : BaseCoreFunctionalTestCase() {

    override fun getAnnotatedClasses(): Array<Class<*>> {
        return arrayOf(User::class.java)
    }

    @Test
    fun givenUser_whenSaved_thenFound() {
        doInHibernate<Unit>(({ this.sessionFactory() }), { session: Session ->
            val personToSave = User("John", "John123")
            session.persist(personToSave)
            val personFound = session.find(User::class.java, personToSave.id)
            session.refresh(personFound)
            assertTrue(personToSave == personFound)
        })
    }

}